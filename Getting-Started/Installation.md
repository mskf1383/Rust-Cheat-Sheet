# Installation

Installing `rustup` on Linux or macOS:
```bash
$ curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```

Updating `rustup`:
```bash
$ rustup update
```

Uninstalling `rustup`:
```bash
$ rustup self uninstall
```

Check Rust version:
```bash
$ rustc --version
```
