# Hello, World!

Hello world program:
```rust
fn main() {
    println!("Hello, world!");
}
```

Compiling Rust program using `rustc`:
```bash
$ rustc <file_name>.rs
```
