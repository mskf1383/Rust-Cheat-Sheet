# Hello, Cargo!

Check Cargo version:
```bash
$ cargo --version
```

Creating a project with Cargo:
```bash
$ cargo new <project_name>
```

Building project with Cargo:
```bash
$ cargo build
```

Running project with Cargo:
```bash
$ cargo run
```

Checking project for compilation errors with Cargo:
```bash
$ cargo check
```

Building project for release with Cargo:
```bash
$ cargo build --release
```
