# Variables and Mutability

Define a new variable:
```rust
let <name>: <type> = <value>;
// ex: let my_number: i32 = 42;
```

Define a new mutable variable:
```rust
let mut <name>: <type> = <value>;
// ex: let mut my_number: i32 = 42;
```

Print a variable value:
```rust
println!("{}", <variable_name>);
// ex: println!("{}", my_number);
```

Change a mutable variable value:
```rust
<variable_name> = <new_value>;
// ex: my_number = 53;
```

Define a new constant:
```rust
const <name>: <type> = <value>;
// ex: const MY_NUMBER: i32 = 42;
```

Shadowing a variable:
```rust
let <name>: <type> = <value>;
let <name> = <name> <operator> <something>;
// ex: let my_number: i32 = 42;
//     let my_number = my_number * 2;
```

**NOTE:** 42 is the answer to life, the universe, and everything - *The Hitchhiker's Guide to the Galaxy* - *Douglas Adams*
