# Data Types

## Scalar Types

Integer Types:

Length           | Signed  | Unsigned
---------------- | ------- | --------
8-bit            | `i8`    | `u8`
16-bit           | `i16`   | `u16`
32-bit (Default) | `i32`   | `u32`
64-bit           | `i64`   | `u64`
128-bit          | `i128`  | `u128`
arch             | `isize` | `usize`

Integer literal forms:

Number literals | Example
--------------- | -------
Decimal         | `98_222`
Hex             | `0xff`
Octal           | `0o77`
Binary          | `0b1111_0000`
Byte(`u8` only) | `b'A'`

Floating-Point Types:

Length           | code
---------------- | -----
32-bit           | `f32`
64-bit (Default) | `f64`

Numeric Operations:

Operation      | Operator
-------------- | --------
Addition       | `+`
Subtraction    | `-`
Multiplication | `*`
Division       | `/`
Remainder      | `%`

The Boolean Type:
```rust
let <var_name>: bool = <true_or_false>;
// ex: let t: bool = true;
```

The Character Type:
```rust
let <var_name>: char = '<char>';
// ex: let a: char = 'a';
```

## Compound Types

The Tuple Type:
```rust
let tup: (<type1>, <type2>, <type3>, ...) = (<value1>, <value2>, <value3>, ...);
// ex: let tup: (i32, f64, u8) = (500, 6.4, 1);
```

Destructure a tuple value:
```rust
let (<var1>, <var2>, <var3>, ...) = <your_tuple>;
// ex:
// let tup = (500, 6.4, 1);
// let (x, y, z) = tup;
```

Access to a tuple value by index:
```rust
<tup_name>.<index>
// ex:
// let x: (i32, f64, u8) = (500, 6.4, 1);
// let five_hundred = x.0;
// let six_point_four = x.1;
// let one = x.2;
```

The Array Type:
```rust
let <var_name>: [<arr_items_type>; <length_of_arr>] = [<value1>, <value2>, <value3>, ...];
// ex: let a: [i32; 5] = [1, 2, 3, 4, 5];
```

Creating a array with same values:
```rust
let <var_name> = [<value>; <length>];
// ex: let a = [3; 5];
// Output: [3, 3, 3, 3, 3]
```

Accessing Array Elements by index:
```rust
<arr_name>[<index>]
// ex:
// let a = [1, 2, 3, 4, 5];
// let first = a[0];
// let second = a[1];
```

**NOTE:** Is not necessary to annonate the data type