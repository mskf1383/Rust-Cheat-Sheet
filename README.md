# Rust Cheat Sheet

A cheat sheet for [The Rust Programming Language book](https://doc.rust-lang.org/book/)!

- Getting Started
  - [Installation](//mskf1383/Rust-Cheat-Sheet/src/branch/master/Getting-Started/Installation.md)
  - [Hello, World!](//mskf1383/Rust-Cheat-Sheet/src/branch/master/Getting-Started/Hello-World.md)
  - [Hello, Cargo!](//mskf1383/Rust-Cheat-Sheet/src/branch/master/Getting-Started/Hello-Cargo.md)

- Common Programming Concepts
  - [Variables and Mutability](//mskf1383/Rust-Cheat-Sheet/src/branch/master/Common-Programming-Concepts/Variables-and-Mutability.md)
  - [Data Types](//mskf1383/Rust-Cheat-Sheet/src/branch/master/Common-Programming-Concepts/Data-Types.md)
  
- Comming soon...